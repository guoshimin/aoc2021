#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>

int solve_atoi(char *buf, size_t size) {
  int incs = 0;
  int last = 0x7fffffff;
  off_t i = 0;
  off_t num_start = i;
  char tmp_buf[16];
  while (i < size) {
    if (buf[i] == '\n') {
      int num = 0;
      int n_digits = i - num_start;
      memcpy(tmp_buf, buf + num_start, n_digits);
      tmp_buf[n_digits] = 0;
      num = atoi(tmp_buf);
      incs += num > last;
      last = num;
      num_start = i + 1;
    }
    i++;
  }

  return incs;
}

int main(int argc, char *argv[]) {
  int fd = open(argv[1], O_RDONLY);
  if (fd < 0) {
    fprintf(stderr, "Error opening input: %s\n", strerror(errno));
    return -1;
  }
  struct stat statbuf;
  int rc = fstat(fd, &statbuf);
  if (rc < 0) {
    fprintf(stderr, "Error stat'ing input: %s\n", strerror(errno));
    return -1;
  }
  void *buf = mmap(NULL, statbuf.st_size, PROT_READ, MAP_SHARED, fd, 0);
  if (buf == (void *) -1) {
    fprintf(stderr, "Error mmap'ing input: %s\n", strerror(errno));
    return -1;
  }
  close(fd);

  int res = 0;

  for (int i = 0; i < 100000; i++) {
    res += solve_atoi((char *)buf, statbuf.st_size);
  }
  printf("res: %d\n", res);
  return 0;
}
