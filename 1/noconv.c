#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <byteswap.h>
#include <time.h>
#include <sys/ioctl.h>
#include <linux/perf_event.h>
#include <linux/hw_breakpoint.h>
#include <asm/unistd.h>

int noconv(char *buf, size_t size) {
  int incs = 0;
  int last;
  off_t i = 0;
  int num = 0x0a0a0a0a;
  if (buf[3] == '\n') {
    memcpy(((char *)&num) + 1, buf, 3);
    last = bswap_32(num);
    i = 4;
  } else {
    memcpy(((char *)&num), buf, 4);
    last = bswap_32(num);
    i = 5;
  }

  while (i + 3 < size) {
    if (buf[i + 3] == '\n') {
      memcpy(((char *)&num), buf + i - 1, 4);
      i += 4;
    } else {
      memcpy(((char *)&num), buf + i, 4);
      i += 5;
    }
    num = bswap_32(num);
    incs += num > last;
    last = num;
  }

  return incs;
}

uint64_t rdtsc(){
  unsigned int lo,hi;
  __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
  return ((uint64_t)hi << 32) | lo;
}

void *map_file(char *filename) {
  int fd = open(filename, O_RDONLY);
  if (fd < 0) {
    fprintf(stderr, "Error opening input: %s\n", strerror(errno));
    return NULL;
  }
  struct stat statbuf;
  int rc = fstat(fd, &statbuf);
  if (rc < 0) {
    fprintf(stderr, "Error stat'ing input: %s\n", strerror(errno));
    return NULL;
  }
  void *buf = mmap(NULL, statbuf.st_size, PROT_READ, MAP_SHARED, fd, 0);
  if (buf == (void *) -1) {
    fprintf(stderr, "Error mmap'ing input: %s\n", strerror(errno));
    return NULL;
  }
  close(fd);
  return buf;
}

void get_lines(char *filename, char ***lines, size_t *arr_size, int *num_lines) {
  FILE *fp = fopen(filename, "r");
  int line_idx = 0;
  while (1) {
    char *line_buf = NULL;
    size_t size = 0;
    size_t rc = getline(&line_buf, &size, fp);
    if (rc == -1) {
      if (feof(fp)) {
        *num_lines = line_idx;
        return;
      }
      fprintf(stderr, "Error calling getline: %s. line_idx=%d\n", strerror(errno), line_idx);
      exit(1);
    }
    if (line_idx >= *arr_size) {
      *lines = realloc(*lines, *arr_size * 2);
      *arr_size = *arr_size * 2;
    }
    (*lines)[line_idx] = line_buf;
    line_idx++;
  }
}

void shuffle_lines(char** lines, int num_lines) {
  for (int i = 0; i < num_lines; i++) {
    int chosen = i + rand() % (num_lines - i);
    char *tmp = lines[i];
    lines[i] = lines[chosen];
    lines[chosen] = tmp;
  }
}

void write_to_buf(char **lines, int num_lines, char *buf) {
  off_t offset = 0;
  for (int i = 0; i < num_lines; i++) {
    int len = strlen(lines[i]);
    memcpy(buf + offset, lines[i], len);
    offset += len;
  }
}

static long
perf_event_open(struct perf_event_attr *hw_event, pid_t pid,
                int cpu, int group_fd, unsigned long flags)
{
  int ret;

  ret = syscall(__NR_perf_event_open, hw_event, pid, cpu,
                group_fd, flags);
  return ret;
}

int get_perf_fd(__u64 config, int group_fd) {
  struct perf_event_attr attr;
  memset(&attr, 0, sizeof(struct perf_event_attr));
  attr.type = PERF_TYPE_HARDWARE;
  attr.size = sizeof(struct perf_event_attr);
  attr.config = config;
  attr.disabled = 1;
  attr.exclude_kernel = 1;
  attr.exclude_hv = 1;
  /* attr.read_format = PERF_FORMAT_GROUP; */

  int fd = perf_event_open(&attr, /* pid */ 0, /* cpu */ -1, group_fd, PERF_FLAG_FD_CLOEXEC);
  if (fd == -1) {
    fprintf(stderr, "Error opening perf event %llx\n", attr.config);
    exit(1);
  }
  return fd;
}

uint64_t read_perf_counter(int fd) {
  char buf[1024];
  uint64_t count;
  int rc = read(fd, buf, 1024);
  if (rc < 0) {
    fprintf(stderr, "Error reading perf event: %s\n", strerror(errno));
    exit(-1);
  }
  return *((uint64_t *) buf);
  /* int nr = *((uint64_t *) buf); */
  /* for (int i = 0; i < nr; i++) { */
  /*   values[i] = *(uint64_t *)(buf + 8 + i * 8); */
  /* } */
}

int main(int argc, char *argv[]) {
  time_t t;
  srand((unsigned) time(&t));

  int fd = open(argv[1], O_RDONLY);
  if (fd < 0) {
    fprintf(stderr, "Error opening input: %s\n", strerror(errno));
    return -1;
  }
  struct stat statbuf;
  int rc = fstat(fd, &statbuf);
  if (rc < 0) {
    fprintf(stderr, "Error stat'ing input: %s\n", strerror(errno));
    return -1;
  }

  /* void *buf = map_file(argv[1]); */
  /* if (!buf) { */
  /*   fprintf(stderr, "Error mmap'ing input: %s\n", strerror(errno)); */
  /*   return -1; */
  /* } */

  char *buf = malloc(statbuf.st_size);
  size_t arr_size = 2000;
  char **lines = malloc(sizeof(char *) * arr_size);
  int num_lines;
  get_lines(argv[1], &lines, &arr_size, &num_lines);

  int cycles_fd = get_perf_fd(PERF_COUNT_HW_CPU_CYCLES, -1);
  int instrs_fd = get_perf_fd(PERF_COUNT_HW_INSTRUCTIONS, cycles_fd);
  int branch_misses_fd = get_perf_fd(PERF_COUNT_HW_BRANCH_MISSES, cycles_fd);

  int res = 0;
  uint64_t cycles = 0;
  uint64_t perf_cycles = 0;
  uint64_t perf_instrs = 0;
  uint64_t perf_branch_misses = 0;
  for (int i = 0; i < 100000; i++) {
    shuffle_lines(lines, num_lines);
    write_to_buf(lines, num_lines, buf);

    ioctl(cycles_fd, PERF_EVENT_IOC_RESET, PERF_IOC_FLAG_GROUP);
    ioctl(cycles_fd, PERF_EVENT_IOC_ENABLE, PERF_IOC_FLAG_GROUP);
    uint64_t start = rdtsc();
    res += noconv((char *)buf, statbuf.st_size);
    uint64_t end = rdtsc();
    ioctl(cycles_fd, PERF_EVENT_IOC_DISABLE, PERF_IOC_FLAG_GROUP);

    perf_cycles += read_perf_counter(cycles_fd);
    perf_instrs += read_perf_counter(instrs_fd);
    perf_branch_misses += read_perf_counter(branch_misses_fd);
    cycles += (end - start);
  }
  printf("res: %d\n", res);
  printf("cycles: %lu\n", cycles);
  printf("perf_cycles: %lu\n", perf_cycles);
  printf("perf_instrs: %lu\n", perf_instrs);
  printf("perf_branch_misses: %lu\n", perf_branch_misses);
  return 0;
}
