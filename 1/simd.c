#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <immintrin.h>
#include <byteswap.h>
#include <time.h>
#include <sys/ioctl.h>
#include <linux/perf_event.h>
#include <linux/hw_breakpoint.h>
#include <asm/unistd.h>

static __m256i newlines = {
  0x0a0a0a0a0a0a0a0a,
  0x0a0a0a0a0a0a0a0a,
  0x0a0a0a0a0a0a0a0a,
  0x0a0a0a0a0a0a0a0a,
};

void write_index(int32_t **index, off_t base, uint64_t bits) {
  int cnt = __builtin_popcountl(bits);
  int j = 0;
  for (; j < 16; j++) {
    (*index)[j] = base + __builtin_ctzl(bits);
    bits = _blsr_u64(bits);
  }

  if (__builtin_expect(cnt > 16, 0)) {
    do {
      (*index)[j] = base + __builtin_ctzl(bits);
      bits = _blsr_u64(bits);
      j++;
    } while (j < cnt);
  }
  *index += cnt;
}

uint64_t identify_newlines(const __m256i &block1, const __m256i &block2) {
  __m256i tmp1 = _mm256_cmpeq_epi8(block1, newlines);
  __m256i tmp2 = _mm256_cmpeq_epi8(block2, newlines);
  uint64_t mask1 = (uint32_t) _mm256_movemask_epi8(tmp1);
  uint64_t mask2 = (uint32_t) _mm256_movemask_epi8(tmp2);
  return mask2 << 32 | mask1;
}

__m256i load(const uint8_t *buf) {
  return _mm256_loadu_si256((__m256i const *) buf);
}

void step(const uint8_t *buf, int32_t idx, int32_t **index_tail, uint64_t &prev_bits) {
  __m256i block1 = load(buf);
  __m256i block2 = load(buf+32);
  __m256i block3 = load(buf+64);
  __m256i block4 = load(buf+96);
  u_int64_t cmp_res1 = identify_newlines(block1, block2);
  u_int64_t cmp_res2 = identify_newlines(block3, block4);
  write_index(index_tail, idx - 64, prev_bits);
  prev_bits = cmp_res1;
  write_index(index_tail, idx, prev_bits);
  prev_bits = cmp_res2;
}

void stage1(const uint8_t *buf, size_t len, int32_t *index, int32_t *newline_count) {
  int32_t *index_tail = index;
  u_int64_t prev_bits = 0;
  off_t i = 0;
  for (; i + 127 < len; i += 128) {
    step(buf + i, i, &index_tail, prev_bits);
  }
  if (len != i) {
    uint8_t tailbuf[128];
    memset(tailbuf, 0, 128);
    memcpy(tailbuf, buf + i, len - i);
    step(tailbuf, i, &index_tail, prev_bits);
    i += 128;
  }
  write_index(&index_tail, i - 64, prev_bits);

  *newline_count = index_tail - index;
}

static const __m256i bswap_mask = {
  0x0405060700010203,
  0x0c0d0e0f08090a0b,
  0x0405060700010203,
  0x0c0d0e0f08090a0b,
};

inline void load_8_numbers0(const uint8_t *buf, const int32_t *index, int32_t *numbers) {
  numbers[0] = 0x0a0a0a0a;

  __builtin_memcpy(((uint8_t *) (numbers)) + 4 - index[0], buf, index[0]);

  __builtin_memcpy((void *) (numbers + 1), buf + index[1] - 4, 4);

  __builtin_memcpy((void *) (numbers + 2), buf + index[2] - 4, 4);

  __builtin_memcpy((void *) (numbers + 3), buf + index[3] - 4, 4);

  __builtin_memcpy((void *) (numbers + 4), buf + index[4] - 4, 4);

  __builtin_memcpy((void *) (numbers + 5), buf + index[5] - 4, 4);

  __builtin_memcpy((void *) (numbers + 6), buf + index[6] - 4, 4);

  __builtin_memcpy((void *) (numbers + 7), buf + index[7] - 4, 4);
  for (int i = 0; i < 8; i++) {
    numbers[i] = bswap_32(numbers[i]);
  }
}

inline void load_8_numbers(const uint8_t *buf, const int32_t *index, int idx, int32_t *numbers) {
  __builtin_memcpy((void *) (numbers), buf + index[idx] - 4, 4);

  __builtin_memcpy((void *) (numbers + 1), buf + index[idx+1] - 4, 4);

  __builtin_memcpy((void *) (numbers + 2), buf + index[idx+2] - 4, 4);

  __builtin_memcpy((void *) (numbers + 3), buf +index[idx+3]  - 4, 4);

  __builtin_memcpy((void *) (numbers + 4), buf +index[idx+4]  - 4, 4);

  __builtin_memcpy((void *) (numbers + 5), buf +index[idx+5]  - 4, 4);

  __builtin_memcpy((void *) (numbers + 6), buf +index[idx+6]  - 4, 4);

  __builtin_memcpy((void *) (numbers + 7), buf +index[idx+7]  - 4, 4);
  for (int i = 0; i < 8; i++) {
    numbers[i] = bswap_32(numbers[i]);
  }
}

__m256i _mm256_shift_left_1(__m256i a) {
  __m256i zero = _mm256_setzero_si256();
  __m256i a_high = _mm256_permute2x128_si256(zero, a, 0x03);  // top half of a move to lower
  return _mm256_alignr_epi8(a_high, a, 1);
}

void stage2(const uint8_t *buf, const int32_t *index, int32_t num_newlines, int32_t *numbers) {
  load_8_numbers0(buf, index, numbers);
  for (int i = 8; i + 7 < num_newlines; i+=8) {
    load_8_numbers(buf, index, i, numbers + i);
  }
}

int stage3(const uint8_t *buf, int32_t num_newlines, const int32_t *numbers) {
   int i;
   int incs = 0;
   for (i = 1; i + 7 < num_newlines; i+=8) {
     __m256i b1 = _mm256_loadu_si256((const __m256i *) (numbers + i - 1));
     __m256i b2 = _mm256_loadu_si256((const __m256i *) (numbers + i));
     __m256i cmp = _mm256_cmpgt_epi32(b2, b1);
     int bits = _mm256_movemask_ps(_mm256_castsi256_ps(cmp));
     incs += __builtin_popcount(bits);
   }
   while (i < num_newlines) {
     incs += numbers[i] > numbers[i - 1];
     i++;
   }
   return incs;
}

uint64_t rdtsc(){
  unsigned int lo,hi;
  __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
  return ((uint64_t)hi << 32) | lo;
}

void *map_file(char *filename) {
  int fd = open(filename, O_RDONLY);
  if (fd < 0) {
    fprintf(stderr, "Error opening input: %s\n", strerror(errno));
    return NULL;
  }
  struct stat statbuf;
  int rc = fstat(fd, &statbuf);
  if (rc < 0) {
    fprintf(stderr, "Error stat'ing input: %s\n", strerror(errno));
    return NULL;
  }
  void *buf = mmap(NULL, statbuf.st_size, PROT_READ, MAP_SHARED, fd, 0);
  if (buf == (void *) -1) {
    fprintf(stderr, "Error mmap'ing input: %s\n", strerror(errno));
    return NULL;
  }
  close(fd);
  return buf;
}

void get_lines(char *filename, char ***lines, size_t *arr_size, int *num_lines) {
  FILE *fp = fopen(filename, "r");
  int line_idx = 0;
  while (1) {
    char *line_buf = NULL;
    size_t size = 0;
    size_t rc = getline(&line_buf, &size, fp);
    if (rc == -1) {
      if (feof(fp)) {
        *num_lines = line_idx;
        return;
      }
      fprintf(stderr, "Error calling getline: %s. line_idx=%d\n", strerror(errno), line_idx);
      exit(1);
    }
    if (line_idx >= *arr_size) {
      *lines = (char **) realloc(*lines, *arr_size * 2);
      *arr_size = *arr_size * 2;
    }
    (*lines)[line_idx] = line_buf;
    line_idx++;
  }
}

void shuffle_lines(char** lines, int num_lines) {
  for (int i = 0; i < num_lines; i++) {
    int chosen = i + rand() % (num_lines - i);
    char *tmp = lines[i];
    lines[i] = lines[chosen];
    lines[chosen] = tmp;
  }
}

void write_to_buf(char **lines, int num_lines, char *buf) {
  off_t offset = 0;
  for (int i = 0; i < num_lines; i++) {
    int len = strlen(lines[i]);
    memcpy(buf + offset, lines[i], len);
    offset += len;
  }
}

int simd(const uint8_t *buf, size_t size, int *index, int *numbers) {
  int newline_count;
  stage1(buf, size, index, &newline_count);
  stage2(buf, index, newline_count, numbers);
  return stage3(buf, newline_count, numbers);
}

static long
perf_event_open(struct perf_event_attr *hw_event, pid_t pid,
                int cpu, int group_fd, unsigned long flags)
{
  int ret;

  ret = syscall(__NR_perf_event_open, hw_event, pid, cpu,
                group_fd, flags);
  return ret;
}

int get_perf_fd(__u64 config, int group_fd) {
  struct perf_event_attr attr;
  memset(&attr, 0, sizeof(struct perf_event_attr));
  attr.type = PERF_TYPE_HARDWARE;
  attr.size = sizeof(struct perf_event_attr);
  attr.config = config;
  attr.disabled = 1;
  attr.exclude_kernel = 1;
  attr.exclude_hv = 1;
  /* attr.read_format = PERF_FORMAT_GROUP; */

  int fd = perf_event_open(&attr, /* pid */ 0, /* cpu */ -1, group_fd, PERF_FLAG_FD_CLOEXEC);
  if (fd == -1) {
    fprintf(stderr, "Error opening perf event %llx\n", attr.config);
    exit(1);
  }
  return fd;
}

uint64_t read_perf_counter(int fd) {
  char buf[1024];
  uint64_t count;
  int rc = read(fd, buf, 1024);
  if (rc < 0) {
    fprintf(stderr, "Error reading perf event: %s\n", strerror(errno));
    exit(-1);
  }
  return *((uint64_t *) buf);
  /* int nr = *((uint64_t *) buf); */
  /* for (int i = 0; i < nr; i++) { */
  /*   values[i] = *(uint64_t *)(buf + 8 + i * 8); */
  /* } */
}

int main(int argc, char *argv[]) {
  time_t t;
  srand((unsigned) time(&t));

  int fd = open(argv[1], O_RDONLY);
  if (fd < 0) {
    fprintf(stderr, "Error opening input: %s\n", strerror(errno));
    return -1;
  }
  struct stat statbuf;
  int rc = fstat(fd, &statbuf);
  if (rc < 0) {
    fprintf(stderr, "Error stat'ing input: %s\n", strerror(errno));
    return -1;
  }
  close(fd);
  /* void *buf = map_file(argv[1]); */
  /* if (!buf) { */
  /*   fprintf(stderr, "Error mmap'ing input: %s\n", strerror(errno)); */
  /*   return -1; */
  /* } */

  char *buf = (char *) malloc(statbuf.st_size);
  size_t arr_size = 2000;
  char **lines = (char **) malloc(sizeof(char *) * arr_size);
  int num_lines;
  get_lines(argv[1], &lines, &arr_size, &num_lines);

  int cycles_fd = get_perf_fd(PERF_COUNT_HW_CPU_CYCLES, -1);
  int instrs_fd = get_perf_fd(PERF_COUNT_HW_INSTRUCTIONS, cycles_fd);
  int branch_misses_fd = get_perf_fd(PERF_COUNT_HW_BRANCH_MISSES, cycles_fd);

  int res = 0;
  uint64_t cycles = 0;
  uint64_t perf_cycles = 0;
  uint64_t perf_instrs = 0;
  uint64_t perf_branch_misses = 0;

  int32_t *index = (int32_t *) malloc(sizeof(int32_t) * statbuf.st_size / 2);
  int32_t* numbers = (int32_t *) malloc(sizeof(int32_t) * statbuf.st_size / 4);
  for (int i = 0; i < 100000; i++) {
    shuffle_lines(lines, num_lines);
    write_to_buf(lines, num_lines, buf);

    ioctl(cycles_fd, PERF_EVENT_IOC_RESET, PERF_IOC_FLAG_GROUP);
    ioctl(cycles_fd, PERF_EVENT_IOC_ENABLE, PERF_IOC_FLAG_GROUP);
    uint64_t start = rdtsc();
    int32_t newline_count;
    res += simd((const uint8_t *) buf, statbuf.st_size, index, numbers);
    /* stage1((const uint8_t *) buf, statbuf.st_size, index, &newline_count); */
    /* stage2((const uint8_t *) buf, index, newline_count, numbers); */
    /* res += stage3((const uint8_t *) buf, newline_count, numbers); */
    uint64_t end = rdtsc();
    ioctl(cycles_fd, PERF_EVENT_IOC_DISABLE, PERF_IOC_FLAG_GROUP);

    perf_cycles += read_perf_counter(cycles_fd);
    perf_instrs += read_perf_counter(instrs_fd);
    perf_branch_misses += read_perf_counter(branch_misses_fd);
    cycles += (end - start);
  }
  printf("res: %d\n", res);
  printf("cycles: %lu\n", cycles);
  printf("perf_cycles: %lu\n", perf_cycles);
  printf("perf_instrs: %lu\n", perf_instrs);
  printf("perf_branch_misses: %lu\n", perf_branch_misses);
  return 0;
}
