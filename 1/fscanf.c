#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>

int baseline(FILE* fp) {
  int num, len;
  off_t offset = 0;
  int last_num = 0x7fffffff;
  int incs = 0;
  int epoch = 0;
  while (1) {
    int c = fscanf(fp, "%d", &num);
    if (c == EOF) {
      break;
    }
    incs += num > last_num;
    last_num = num;
  }
  return incs;
}

int main(int argc, char *argv[]) {
  FILE *fp = fopen(argv[1], "r");
  if (!fp) {
    fprintf(stderr, "Error opening input: %s\n", strerror(errno));
    return -1;
  }
  int res = 0;
  for (int i = 0; i < 100000; i++) {
    res += baseline(fp);
    rewind(fp);
  }
  printf("%d\n", res);
  fclose(fp);
  return 0;
}
